for i in *ranges*
do
taxa="$(echo $i | cut -d'_' -f1)"
cp template_FBDRMatrix_m3_sm_norm.Rev $taxa\_FBDRMatrix_m3.Rev
sed -i '' -e s/TAXA_/$taxa\_/g $taxa\_FBDRMatrix_m3.Rev
done
