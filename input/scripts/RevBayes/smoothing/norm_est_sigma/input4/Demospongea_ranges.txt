taxon	min	max
Annaecoelia	203.35	232
Russospongia	203.35	215.25
Tolminothalamia	226.5	239.25
Vesicocaulis	207.95	239.25
Yukonella	207.95	215.25
Astrosclera	172.2	215.25
Aliabadia	207.95	215.25
Anguispongia	207.95	215.25
Cavusonella	215.25	239.25
Molengraaffia	203.35	232
Hartmanina	232	239.25
Colospongia	203.35	239.25
Hajarispongia	207.95	215.25
Kashanella	207.95	232
Parauvanella	203.35	239.25
Platythalamiella	203.35	239.25
Antalythalamia	207.95	215.25
Cryptocoelia	203.35	239.25
Sphaerothalamia	207.95	215.25
Enoplocoelia	215.25	239.25
Girtycoelia	215.25	239.25
Henricellum	207.95	239.25
Phraethalamia	207.95	239.25
Polyedra	203.35	239.25
Sollasia	215.25	239.25
Globucatenula	207.95	215.25
Cystothalamia	207.95	239.25
Diecithalamia	226.5	239.25
Delijania	207.95	215.25
Prosiphonella	226.5	232
Olangocoelia	239.25	239.25
Palermocoelia	203.35	215.25
Asiphothalamia	207.95	215.25
Parastylothalamia	207.95	215.25
Euepirrhysia	226.5	232
Grossotubenella	215.25	239.25
Leiospongia	232	239.25
Permocorynella	207.95	232
Precorynella	203.35	239.25
Preperonidella	203.35	239.25
Salzburgia	203.35	239.25
Amblysiphonella	203.35	239.25
Calabrisiphonella	207.95	215.25
Discosiphonella	207.95	239.25
Polycystocoelia	207.95	239.25
Himatella	226.5	232
Marawandia	207.95	215.25
Sestrostomella	172.2	239.25
Battaglia	207.95	215.25
Deningeria	207.95	239.25
Panormida	207.95	215.25
Paradeningeria	203.35	232
Sahraja	203.35	215.25
Senowbaridaryana	203.35	232
Seranella	203.35	232
Solenolmia	203.35	239.25
Welteria	203.35	226.5
Lutia	207.95	215.25
Prestellispongia	207.95	215.25
Vermispongiella	207.95	215.25
Musandamia	207.95	215.25
Naybandella	207.95	215.25
Pamirothalamia	207.95	215.25
Pamiroverticillites	207.95	215.25
Sphaeroverticillites	207.95	215.25
Thaumastocoelia	203.35	239.25
Madonia	207.95	215.25
Actinospongiella	203.35	203.35
Paelospongia	207.95	239.25
Atrochaetetes	207.95	232
Cassianochaetes	232	232
Ceratoporella	172.2	239.25
Reticulocoelia	207.95	232
Cladocoropsis	172.2	199.3
Murania	172.2	215.25
Parastromatopora	172.2	215.25
Neuropora	172.2	201.3
Actinofungia	226.5	226.5
Circopora	203.35	239.25
Cylicoopsis	172.2	215.25
Ellipsactinia	172.2	215.25
Lamellata	203.35	232
Lithopora	172.2	215.25
Pamirostroma	207.95	215.25
Pantokratoria	226.5	239.25
Stromatoporellata	203.35	232
Stromatoporina	172.2	232
Stromatostroma	203.35	232
Zlambachella	203.35	203.35
Aka	232	232
Cassianothalamia	215.25	232
Alpinothalamia	207.95	239.25
Celyphia	207.95	239.25
Follicatena	203.35	239.25
Jablonskyia	226.5	239.25
Leinia	226.5	239.25
Paravesicocaulis	203.35	239.25
Pseudouvanella	207.95	215.25
Uvanella	203.35	239.25
Ceotinella	226.5	239.25
Zardinia	226.5	239.25
Cassianostroma	207.95	232
Hispidopetra	172.2	232
Petrosistroma	232	232
Costamorpha	203.35	215.25
Hymedesmia	232	232
Bauneia	172.2	215.25
Blastochaetetes	172.2	226.5
Chaetetes	187.55	239.25
Pseudoseptifer	172.2	215.25
Ptychochaetetes	172.2	212.875
Keriocoelia	207.95	232
Meandripetra	226.5	232
Pamirochaetetes	226.5	226.5
Precaratoporella	203.35	239.25
Pseudochaetetes	172.2	212.875
Sclerocoelia	226.5	232
Cheilosporites	203.35	215.25
Neoguadalupia	215.25	239.25
Cribrothalamia	203.35	215.25
Gigantothalamia	207.95	215.25
Zanklithalamia	203.35	215.25
Cinnabaria	207.95	226.5
Faniathalamia	203.35	226.5
Nevadathalamia	212.875	215.25
Polysiphospongia	203.35	215.25
Polytholosia	207.95	232
Stylothalamia	172.2	239.25
Verticillites	172.2	239.25
Platysphaerocoelia	207.95	215.25
Dactylites	176.2	239.25
Burgundia	172.2	232
Criccophorina	203.35	203.35
Radiocella	207.95	215.25
Tabasia	207.95	215.25
Lovcenipora	203.35	232
Verrucospongia	172.2	232
