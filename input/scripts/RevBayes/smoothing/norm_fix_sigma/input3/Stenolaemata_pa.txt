taxon	int1	int2	int3	int4	int5	int6	int7	int8	int9	int10
Diastopora	0	1	0	0	0	1	0	0	0	0	
Entalophora	0	1	0	0	0	0	0	0	0	0	
Microeciella	0	0	1	0	0	0	0	0	0	0	
Stomatopora	0	0	0	0	0	0	1	0	1	0	
Braiesopora	0	0	0	0	0	0	0	0	1	0	
Buria	0	0	0	0	0	0	0	1	0	0	
Corynotrypoides	0	0	0	0	0	0	0	0	1	0	
Ramia	0	0	0	0	0	0	0	0	1	0	
Reptomultisparsa	0	0	0	0	0	0	1	0	0	0	
Reptonodicava	0	0	0	0	0	0	0	1	0	0	
Cystitrypa	0	0	0	0	0	0	0	0	1	0	
Cassianopora	0	0	0	0	0	0	0	1	0	0	
Cyclotrypa	0	0	0	0	0	0	0	1	0	0	
Tebitopora	0	0	0	0	0	0	0	1	1	1	
Reptonoditrypa	0	0	0	0	0	0	1	1	0	1	
Monotrypa	0	0	0	0	0	0	0	0	1	0	
Paralioclema	0	0	0	0	0	0	0	1	1	1	
Styloclema	0	0	0	0	0	0	0	1	0	0	
Arcticopora	0	0	0	0	0	0	0	1	1	1	
Dyscritellopsis	0	0	0	0	0	0	0	0	1	0	
Zozariella	0	0	0	0	0	0	0	1	1	0	
Dyscritella	0	0	0	0	0	0	1	1	1	1	
Pseudobatostomella	0	0	0	0	0	0	0	0	1	1	
Diaphragmopora	0	0	0	0	0	0	0	1	0	0	
Stenodiscus	0	0	0	0	0	0	0	1	0	0	
Flustra	0	0	0	0	0	0	0	0	1	0	
Leioclema	0	0	0	0	0	0	0	0	1	0	
