# read stratigraphic ranges
taxa = readTaxonData(file = "combined_ranges.txt")

# interval boundaries
timeline <- v(170.3, 174.1, 182.7, 190.8, 199.3, 201.3, 205.4, 221.0, 237.0)

mvi = 1

alpha <- 10

# smoothing parameters

# for fixed sigma
sigma <- 1/alpha
      
# specify FBDR model parameters -> youngest interval = 1
for(i in 1:(timeline.size() + 1)){

  # sample value (in log space) for the first interval
  if(i == 1){
    
    m[i] ~ dnNorm(0, sigma)
    l[i] ~ dnNorm(0, sigma)

  } else { # for older intervals

    m[i] ~ dnNorm( m[i - 1], sigma )
    l[i] ~ dnNorm( l[i - 1], sigma )

  }

  moves[mvi++] = mvScale(m[i], tune = true)          
  moves[mvi++] = mvScale(l[i], tune = true)

  # transform values from log scale
  mu[i] := exp( m[i] )
  lambda[i] := exp( l[i] )
  
  div[i] := lambda[i] - mu[i]
  turnover[i] := mu[i]/lambda[i]
  
  psi[i] ~ dnExp(alpha)

  moves[mvi++] = mvScale(psi[i], tune = true)

}     

rho <- 0

# model 1
bd ~ dnFBDRMatrix(taxa=taxa, lambda=lambda, mu=mu, psi=psi, rho=rho, timeline=timeline)

moves[mvi++] = mvMatrixElementScale(bd, lambda = 0.01, weight=taxa.size())
moves[mvi++] = mvMatrixElementScale(bd, lambda = 0.1, weight=taxa.size())
moves[mvi++] = mvMatrixElementScale(bd, lambda = 1, weight=taxa.size())
      
moves[mvi++] = mvMatrixElementSlide(bd, delta = 0.01, weight=taxa.size())
moves[mvi++] = mvMatrixElementSlide(bd, delta = 0.1, weight=taxa.size())
moves[mvi++] = mvMatrixElementSlide(bd, delta = 1, weight=taxa.size())
      
mymodel = model(bd)

# add monitors
mni = 1

monitors[mni++] = mnScreen(lambda, mu, psi, printgen=4000)
monitors[mni++] = mnModel(filename = "output/combined_m2.log", printgen=40)

# monitors to print RevGagets input
monitors[mni++] = mnFile(filename = "output/combined_m2_speciation_rates.log", lambda, printgen=40)
monitors[mni++] = mnFile(filename = "output/combined_m2_speciation_time.log", timeline, printgen=40)
monitors[mni++] = mnFile(filename = "output/combined_m2_extinction_rates.log", mu, printgen=40)
monitors[mni++] = mnFile(filename = "output/combined_m2_extinction_time.log", timeline, printgen=40)
monitors[mni++] = mnFile(filename = "output/combined_m2_sampling_rates.log", psi, printgen=40)
monitors[mni++] = mnFile(filename = "output/combined_m2_sampling_time.log", timeline, printgen=40)

# run the analysis
mymcmc = mcmc(mymodel, moves, monitors, moveschedule = "random")
mymcmc.run(400000)

q()

