# read stratigraphic ranges
taxa = readTaxonData(file = "Echinoidea_ranges.txt")

# interval boundaries
timeline <- v(170.30, 174.10, 178.30, 182.70, 187.55, 190.80, 195.30, 199.30, 200.10, 201.30, 205.40, 210.50, 215.25, 221.00, 232.00, 237.00)

mvi = 1

alpha <- 10
      
# specify FBDR model parameters
for(i in 1:(timeline.size() + 1))
{
      
  mu[i] ~ dnExp(alpha)
  lambda[i] ~ dnExp(alpha)
  psi[i] ~ dnExp(alpha)
      
  div[i] := lambda[i] - mu[i]
  turnover[i] := mu[i]/lambda[i]
      
  moves[mvi++] = mvScale(mu[i], lambda = 0.01)
  moves[mvi++] = mvScale(mu[i], lambda = 0.1)
  moves[mvi++] = mvScale(mu[i], lambda = 1)
      
  moves[mvi++] = mvScale(lambda[i], lambda = 0.01)
  moves[mvi++] = mvScale(lambda[i], lambda = 0.1)
  moves[mvi++] = mvScale(lambda[i], lambda = 1)
      
  moves[mvi++] = mvScale(psi[i], lambda = 0.01)
  moves[mvi++] = mvScale(psi[i], lambda = 0.1)
  moves[mvi++] = mvScale(psi[i], lambda = 1)
}
      
rho <- 0
# model 2
bd ~ dnFBDRMatrix(taxa=taxa, lambda=lambda, mu=mu, psi=psi, rho=rho, timeline=timeline)

moves[mvi++] = mvMatrixElementScale(bd, lambda = 0.01, weight=taxa.size())
moves[mvi++] = mvMatrixElementScale(bd, lambda = 0.1, weight=taxa.size())
moves[mvi++] = mvMatrixElementScale(bd, lambda = 1, weight=taxa.size())
      
moves[mvi++] = mvMatrixElementSlide(bd, delta = 0.01, weight=taxa.size())
moves[mvi++] = mvMatrixElementSlide(bd, delta = 0.1, weight=taxa.size())
moves[mvi++] = mvMatrixElementSlide(bd, delta = 1, weight=taxa.size())
      
mymodel = model(bd)

# add monitors
mni = 1

monitors[mni++] = mnScreen(lambda, mu, psi, printgen=4000)
monitors[mni++] = mnModel(filename = "output/Echinoidea_m2b.log", printgen=40)

# monitors to print RevGagets input
monitors[mni++] = mnFile(filename = "output/Echinoidea_m2b_speciation_rates.log", lambda, printgen=40)
monitors[mni++] = mnFile(filename = "output/Echinoidea_m2b_speciation_time.log", timeline, printgen=40)
monitors[mni++] = mnFile(filename = "output/Echinoidea_m2b_extinction_rates.log", mu, printgen=40)
monitors[mni++] = mnFile(filename = "output/Echinoidea_m2b_extinction_time.log", timeline, printgen=40)
monitors[mni++] = mnFile(filename = "output/Echinoidea_m2b_sampling_rates.log", psi, printgen=40)
monitors[mni++] = mnFile(filename = "output/Echinoidea_m2b_sampling_time.log", timeline, printgen=40)
# run the analysis
mymcmc = mcmc(mymodel, moves, monitors, moveschedule = "random")
mymcmc.run(400000)

q()

