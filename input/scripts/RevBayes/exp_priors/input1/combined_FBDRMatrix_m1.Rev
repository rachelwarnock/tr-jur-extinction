# read stratigraphic ranges
taxa = readTaxonData(file = "combined_r.txt")

# read fossil counts
k <- readDataDelimitedFile(file = "combined_p.txt", header = true, rownames = true)

# interval boundaries
timeline <- v(170.3, 174.1, 182.7, 190.8, 199.3, 201.3, 205.4, 221.0, 237.0)

mvi = 1

alpha <- 10
      
# specify FBDR model parameters
for(i in 1:(timeline.size() + 1))
{
      
  mu[i] ~ dnExp(alpha)
  lambda[i] ~ dnExp(alpha)
  psi[i] ~ dnExp(alpha)
      
  div[i] := lambda[i] - mu[i]
  turnover[i] := mu[i]/lambda[i]
      
  moves[mvi++] = mvScale(mu[i], lambda = 0.01)
  moves[mvi++] = mvScale(mu[i], lambda = 0.1)
  moves[mvi++] = mvScale(mu[i], lambda = 1)
      
  moves[mvi++] = mvScale(lambda[i], lambda = 0.01)
  moves[mvi++] = mvScale(lambda[i], lambda = 0.1)
  moves[mvi++] = mvScale(lambda[i], lambda = 1)
      
  moves[mvi++] = mvScale(psi[i], lambda = 0.01)
  moves[mvi++] = mvScale(psi[i], lambda = 0.1)
  moves[mvi++] = mvScale(psi[i], lambda = 1)
}
      
rho <- 0
# model 1
bd ~ dnFBDRMatrix(taxa=taxa, lambda=lambda, mu=mu, psi=psi, rho=rho, timeline=timeline, k=k)

moves[mvi++] = mvMatrixElementScale(bd, lambda = 0.01, weight=taxa.size())
moves[mvi++] = mvMatrixElementScale(bd, lambda = 0.1, weight=taxa.size())
moves[mvi++] = mvMatrixElementScale(bd, lambda = 1, weight=taxa.size())
      
moves[mvi++] = mvMatrixElementSlide(bd, delta = 0.01, weight=taxa.size())
moves[mvi++] = mvMatrixElementSlide(bd, delta = 0.1, weight=taxa.size())
moves[mvi++] = mvMatrixElementSlide(bd, delta = 1, weight=taxa.size())
      
mymodel = model(bd)

# add monitors
mni = 1

monitors[mni++] = mnScreen(lambda, mu, psi, printgen=4000)
monitors[mni++] = mnModel(filename = "output/combined_m1.log", printgen=40)

# monitors to print RevGagets input
monitors[mni++] = mnFile(filename = "output/combined_m1_speciation_rates.log", lambda, printgen=40)
monitors[mni++] = mnFile(filename = "output/combined_m1_speciation_time.log", timeline, printgen=40)
monitors[mni++] = mnFile(filename = "output/combined_m1_extinction_rates.log", mu, printgen=40)
monitors[mni++] = mnFile(filename = "output/combined_m1_extinction_time.log", timeline, printgen=40)
monitors[mni++] = mnFile(filename = "output/combined_m1_sampling_rates.log", psi, printgen=40)
monitors[mni++] = mnFile(filename = "output/combined_m1_sampling_time.log", timeline, printgen=40)
# run the analysis
mymcmc = mcmc(mymodel, moves, monitors, moveschedule = "random")
mymcmc.run(400000)

q()

